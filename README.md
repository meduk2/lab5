# Lab5
##################################################
# Name:         Michael Edukonis
# UIN:          677141300
# email:        meduk2@illinois.edu
# class:        CS437
# assignment:   Lab5
# date:         4/22/2023
##################################################<br>
README<br>
MEDUK2 Lab5 Animal Tagging 2023-04-15.mp4 - video showing tagging of 26 animals in simulation<br>
meduk2_lab5_design-considerations.pdf - Lab 5 writeup<br>
simulation_2023_4_15_15_1_31_READABLE.txt - readable log from simulation<br>
process_log.py - script to parse the readable log<br>
processed_log.csv - output from above script<br>



