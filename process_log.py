##################################################
# Name:         Michael Edukonis
# UIN:          677141300
# email:        meduk2@illinois.edu
# class:        CS437
# assignment:   Lab5
# date:         4/22/2023
##################################################
#process_log.py - script will take as input, the readable logs from Savanna.exe 
#virtual world and parse into a CSV file for processing in excel or python
#
#Required adjustments:
#Line 24 "result" is the list of columns that will be in the csv.  This should
#match what is in the log and this will depend on the sensors used in the 
#circuit simulator.
#
#Lines 49-59 will also depend on the sensors used.  Adjust accordingly.

import pandas as pd
import numpy as np
from io import StringIO
import matplotlib.pyplot as plt

decimals = 1        #for rounding numbers
animal = ''
animal_id = ''
result = "animal,animal_id,time_stamp,oxygen_saturation,heart_rate,temperature,lat,long,light\n"

with open("simulation_2023_4_15_15_1_31_READABLE.txt", "r") as file:
    for line in file:
        if not line.isspace():
            if line[0] == '=':
                line2 = line.split(' ')
                line3 = line2[1].split(':')
                animal = line3[0]
                animal_id = line3[1]
                line = ''
            elif line[0] == '-':
                line = line.replace('-- ', animal+','+animal_id+',')
                line = line.replace('\n', ',')
                line = line.replace(': ', ':')
                line = line.replace('Timestamp:', '')
            elif line[3] != 'g':
                line = line.replace('\n', ',')
                line = line.replace(', "', ',"')
                line = line.replace('"', '')
                line = line.replace(': ', ':')
                line = line.replace('oxygen_saturation:', '')
                line = line.replace('heart_rate:', '')
                line = line.replace('temperature:', '')
                line = line.replace('location:', '')
                line = line.replace('light:', '')
                line = line.replace('[', '')
                line = line.replace(']', '')
            else:
                line = line.replace(': ', ':')
                line = line.replace('"', '')
                line = line.replace('light:', '')
            result += line

    file.seek(0)
    file.close()
    df = pd.read_csv(StringIO(result), index_col=0)
    
    
    #Graph - compare light level vs temperature - uncomment to run.
    '''x = df.drop_duplicates(['time_stamp']).sort_values(by='light')
    plt.title("Air temp at various light levels")
    plt.xlabel('Light level', fontsize=18)
    plt.ylabel('Air Temperature', fontsize=16)
    plt.plot(x['light'], x['temperature'], '-')
    plt.show()'''
    
    #Round off some very long doubles
    #///////////////////////////////////////////////////////////////////////////////////
    df['temperature'] = df['temperature'].apply(lambda x: round(x, decimals))
    df['light'] = df['light'].apply(lambda x: round(x, decimals))
    
    #insert needed columns - will calculate and fill next
    #///////////////////////////////////////////////////////////////////////////////////
    df.insert(loc=8, column = 'distance_traveled', value = '')
    df.insert(loc=9, column = 'speed', value = '')
    #df.insert(loc=10, column = 'std_dev', value = '')
    #df.insert(loc=11, column = 'avg_speed', value = '')
    
    #Calculate and fill in distance traveled at each step
    #///////////////////////////////////////////////////////////////////////////////////
    dx = (df['lat'] - df.groupby('animal_id')['lat'].shift())
    dy = (df['long'] - df.groupby('animal_id')['long'].shift())
    df['distance_traveled'] = np.sqrt(dx**2 + dy**2)
    df['distance_traveled'] = df['distance_traveled'].fillna(0)
    df['distance_traveled'] = df['distance_traveled'].apply(lambda x: round(x, decimals))
    #///////////////////////////////////////////////////////////////////////////////////

    #Calculate and fill in speed at each time step
    #///////////////////////////////////////////////////////////////////////////////////
    time = (df['time_stamp'] - df.groupby('animal_id')['time_stamp'].shift())
    distance = df['distance_traveled']
    df['speed'] = distance/time
    df['speed'] = df['speed'].fillna(0)
    df['speed'] = df['speed'].apply(lambda x: round(x, decimals))
    
    #Backup to CSV file
    #///////////////////////////////////////////////////////////////////////////////////
    df.to_csv('processed_log.csv')

    #Calculate stats
    #///////////////////////////////////////////////////////////////////////////////////
    #mean_df = df.groupby('animal_id')['speed'].mean().to_frame('mean_speed')
    #std_df = df.groupby('animal_id')['speed'].std().to_frame('std_dev')

    #df2 = pd.merge(pd.merge(df, std_df, on='animal_id', how='inner'), mean_df, on='animal_id', how='inner')
    #std_df = df.groupby('animal')['speed'].std().to_dict()
    #mean_df = df.groupby('animal')['speed'].mean().to_dict()
    
    #zebra_mean = mean_df['Zebra']
    #zebra_std = std_df['Zebra']
  
    #Seperate out the zebras into a sub frame
    #df5 = df.groupby('animal').apply(lambda x: x[x['animal'] == 'Zebra' ])
    #print(df5)
    
    #remove speed duplicates
    #df5 = df.drop_duplicates(['speed'])
    
    # Calculating probability density function (PDF)
    #pdf = stats.norm.pdf(df5["speed"].sort_values(), zebra_mean, zebra_std)
    #cdf = np.cumsum(pdf)
    #print(pdf)
    
    #//////////////////////////////////////
    #CDF
    #/////////////////////////////////////
    #x = np.sort(df5["speed"])
    #y = np.arange(len(x))/float(len(x))
    #plt.plot(x,y)
    #plt.show()
    
    #df.to_csv('updated_output.csv')
      
    #heart_rates = df.groupby('animal').agg({'heart_rate': ['mean', 'min', 'max']}).values.tolist()
    
    #Graph min, mean, max heartrates - working
    '''heart_rates = df.groupby('animal').agg({'heart_rate': ['min', 'mean', 'max']}).reset_index()
    heart_rates.plot(x='animal', y='heart_rate', kind='bar')
    plt.title("Animal Heart Rates")
    plt.show()'''
   
    #comparison speeds vs heart rate - zebras only
    '''df6 = df5.drop_duplicates(['speed'])
    #print(df6)
    plt.plot(df6['speed'], df6['heart_rate'], 'o')
    plt.title("Zebra Heart Rates at Various Speeds")
    plt.xlabel('Movement Speed', fontsize=18)
    plt.ylabel('Heart Rate', fontsize=16)
    plt.show()
    df6.to_csv('updated_output.csv')'''
    
    '''#comparison light levels vs temp
    df6 = df.drop_duplicates(['light'])
    plt.plot(df6['light'], df6['temperature'], '-')
    plt.show()
    #print(df6)'''


    #for jupyter notebook demonstration
    '''import pandas as pd
    import numpy as np
    import matplotlib.pyplot as plt

    df = pd.read_csv('processed_log.csv')

    heart_rates = df.groupby('animal').agg({'heart_rate': ['min', 'mean', 'max']}).reset_index()
    heart_rates.plot(x='animal', y='heart_rate', kind='bar')
    plt.title("Animal Heart Rates")
    plt.show()'''
